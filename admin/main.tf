terraform {
  required_version = "~>0.12.26"
  backend "s3" {
    bucket = "madpy-terraform-remote-state"
    key    = "magpy/admin.tfstate"
    region = "us-west-2"
  }
}

data "aws_caller_identity" "current" {}

provider "aws" {
  region  = "us-west-2"
  version = "~>2.63.0"
}

provider "gitlab" {
  version = "~>2.8.0"
}

resource "aws_s3_bucket" "remote_state" {
  bucket        = "madpy-terraform-remote-state"
  force_destroy = true
}
