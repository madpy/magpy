output "automation_user_access_key_id" {
  value = aws_iam_access_key.automation_user.id
}

output "automation_user_secret_access_key" {
  value     = aws_iam_access_key.automation_user.secret
  sensitive = true
}

output "deployment_role_arn" {
  value = aws_iam_role.deployment_role.arn
}
