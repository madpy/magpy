resource "aws_iam_user" "automation_user" {
  name          = "madpy-automation-user"
  force_destroy = true
}

resource "aws_iam_access_key" "automation_user" {
  user = aws_iam_user.automation_user.name
}

resource "aws_iam_policy" "assume_deployment_role" {
  name   = "AssumeDeploymentRole"
  policy = data.aws_iam_policy_document.automation_user.json
}

data "aws_iam_policy_document" "automation_user" {
  statement {
    sid       = "AssumeDeploymentRole"
    actions   = ["sts:AssumeRole"]
    resources = [aws_iam_role.deployment_role.arn]
  }
}

resource "aws_iam_policy" "build_ecr_images" {
  name   = "BuildECRImages"
  policy = data.aws_iam_policy_document.build_ecr_images.json
}

data "aws_iam_policy_document" "build_ecr_images" {
  statement {
    sid       = "GetAuthorizationToken"
    actions   = ["ecr:GetAuthorizationToken"]
    resources = ["*"]
  }

  statement {
    sid = "BuildECRImages"
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:CompleteLayerUpload",
      "ecr:DescribeImages",
      "ecr:DescribeRepositories",
      "ecr:GetAuthorizationToken",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:InitiateLayerUpload",
      "ecr:ListImages",
      "ecr:PutImage",
      "ecr:UploadLayerPart",
    ]
    resources = ["arn:aws:ecr:us-west-2:${data.aws_caller_identity.current.account_id}:repository/*"]
  }
}

locals {
  automation_user_policy_arns = [
    aws_iam_policy.assume_deployment_role.arn,
    aws_iam_policy.build_ecr_images.arn,
  ]
}

resource "aws_iam_user_policy_attachment" "automation_user_policies" {
  count      = length(local.automation_user_policy_arns)
  user       = aws_iam_user.automation_user.name
  policy_arn = local.automation_user_policy_arns[count.index]
}