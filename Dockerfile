FROM python:3.9.0a5

RUN pip install --upgrade pip \
    && pip install pipenv==2018.11.26

WORKDIR /magpy
COPY Pipfile* ./

RUN pipenv install --system --deploy

COPY magpy/*.py magpy/

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]