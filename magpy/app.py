from flask import Flask, request

app = Flask(__name__)


@app.route("/", methods=["POST"])
def home():
    print(f"received a post with data: {request.form}")
    return "Hello, World!"
